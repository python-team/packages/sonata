sonata (1.7~b1-2) UNRELEASED; urgency=medium

  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <onovy@debian.org>  Thu, 01 Mar 2018 08:43:38 +0100

sonata (1.7~b1-1) experimental; urgency=medium

  * New maintainer, the mpd packaging team
    - update Vcs-Git, Vcs-Browser metadata
  * New upstream beta version (Closes: #825033)
    - switch from Python 2 to Python 3
    - switch from static GObject bindings (PyGtk) to PyGI
    - requires python3-mpd (see #808824)
    - new upstream website (Closes: #815101)
    - doesn't use deprecated dbus.glib import (Closes: #734492)
    - doesn't use egg.trayicon (Closes: #560979)
    - no longer provides or uses the mmkeys module
    - d/p/fix-cras-on-no-albums.patch: drop, no longer applicable
    - d/p/fix-lyrics-fetching.patch: drop, applied upstream
    - d/p/fix-missing-crossfade.patch: drop, applied upstream
    - d/p/from_upstream__fix-mmkeys.patch: drop, no longer applicable
  * Update debian/copyright
    - Add Apache 2.0 license grant
  * Standards-Version: 4.1.2
    - drop traditional Debian menu entry and its accompanying .xpm file
  * Run dh_install with --fail-missing to avoid failing to install files
  * When removing unwanted documentation from /usr/share/sonata, remove
    the files individually to avoid accidents
  * debian/clean: clean up ./RECORD
  * debian/rules: clean up ./Sonata.egg-info, ./sonata/share/locale
  * Move to debhelper compat level 10
  * Use dh_missing instead of deprecated dh_install --fail-missing
  * debian/gbp.conf: Add

 -- Simon McVittie <smcv@debian.org>  Tue, 12 Dec 2017 10:42:17 +0000

sonata (1.6.2.1-6) unstable; urgency=low

  [ Javi Merino ]
  * Add my DD account
  * Bump standards-version to 3.9.5 (no change needed)
  * Acknowledge NMU
  * Add a Keywords entry to the .desktop file

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Jackson Doak ]
  * Bump debhelper to 9 (partitally enables hardening)

 -- Javi Merino <vicho@debian.org>  Sun, 23 Feb 2014 19:34:05 +0000

sonata (1.6.2.1-5.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix "missing crossfade in status breaks sonata (mpd >=0.18)" by
    applying the patch provided by Mattia Dongili. (Closes: #728704.)

 -- Stephen Kitt <skitt@debian.org>  Sat, 25 Jan 2014 15:29:25 +0100

sonata (1.6.2.1-5) unstable; urgency=low

  * Fix "Breaks gnome-settings-daemon media-keys plugin" by applying a
    patch in quodlibet's version of mmkeys (Closes: #644735)

 -- Javi Merino <cibervicho@gmail.com>  Sun, 16 Oct 2011 22:22:04 +0100

sonata (1.6.2.1-4) unstable; urgency=low

  [ Michal Čihař ]
  * Adjust watch file to new upstream location.
  * Convert to dh_python2.

  [ Javi Merino ]
  * New maintainer. (Closes: #612910: RFA: sonata -- GTK+ client for the
    Music Player Daemon (MPD))
  * Fix ""Fetching lyrics failed" even if there's a lyrics online"
    with a patch provided by Stefan Fleischmann <ck850@web.de> (Closes:
    #631375)
  * Bump standards to 3.9.2.
  * Change Conflicts with Breaks for python-mmkeys, as it is just a
    package split, as explained in Section 7.6 of the Debian Policy

 -- Javi Merino <cibervicho@gmail.com>  Thu, 28 Jul 2011 14:55:12 +0200

sonata (1.6.2.1-3) unstable; urgency=low

  * Again recommend eggtrayicon because it provides more features
    (see: #560979).
  * Fixed crash when it is not possible to get list of albums
    (Closes: #572262).
  * Bump standards to 3.8.4.

 -- Michal Čihař <nijel@debian.org>  Thu, 03 Jun 2010 10:00:44 +0200

sonata (1.6.2.1-2) unstable; urgency=low

  * Prefer gtk.StatusIcon over eggtrayicon (Closes: #560979).
  * Downgrade mmkeys to suggests as the functionality should be handled by Gtk
    in most cases.
  * Adjust deps to use << instead of <.
  * Convert to 3.0 (quilt) source format.

 -- Michal Čihař <nijel@debian.org>  Sun, 03 Jan 2010 14:48:16 +0100

sonata (1.6.2.1-1) unstable; urgency=low

  * New upstream version.
    - Fixes lyricswiki support (Closes: #542366).
    - No need for ZSI as Sonata now does html scraping.
  * Do not depend unconditionally on elementtree, newer python is enough
    (LP: #377706).
  * Bump standards to 3.8.3.
  * Simplify debian/rules by using new dh support for distutils.

 -- Michal Čihař <nijel@debian.org>  Fri, 25 Sep 2009 14:00:02 +0200

sonata (1.6.2-1) unstable; urgency=low

  * New upstream version (Closes: #528045).

 -- Michal Čihař <nijel@debian.org>  Mon, 11 May 2009 09:10:00 +0200

sonata (1.6-1) unstable; urgency=low

  * New upstream version.
    - Fixes changing of cover art (Closes: #480758).
    - Fixes handling options without MPD connection (Closes: #503435).
    - Fixes typos in man page (Closes: #517503).
  * Build only for Python 2.5 or newer, upstream dropped support for older.

 -- Michal Čihař <nijel@debian.org>  Mon, 06 Apr 2009 15:21:27 +0200

sonata (1.5.3-3) unstable; urgency=low

  * We don't need python 2.4 (Closes: #520513).

 -- Michal Čihař <nijel@debian.org>  Fri, 20 Mar 2009 15:33:41 +0100

sonata (1.5.3-2) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Michal Čihař ]
  * Prepare for python2.6 (*-packages in *.install, --install-layout to
    setup.py install).
  * Switch from pycentral to pysupport.
  * Bump standards to 3.8.1 (no changes needed).
  * Update debian/copyright (years, link to GPL-3).
  * Use dh to simplify debian/rules.

 -- Michal Čihař <nijel@debian.org>  Wed, 18 Mar 2009 16:35:16 +0100

sonata (1.5.3-1) unstable; urgency=low

  * New upstream version.
    - Properly handles bad images in cache (Closes: #495475).
    - Includes correct version in main.py (Closes: #491603).
  * Do not make pixmaps executable.

 -- Michal Čihař <nijel@debian.org>  Wed, 17 Sep 2008 01:00:20 +0200

sonata (1.5.2-2) unstable; urgency=low

  * Recommend python-eggtrayicon instead of full python-gnome2-extras
    (Closes: #485323).

 -- Michal Čihař <nijel@debian.org>  Mon, 09 Jun 2008 08:45:55 +0200

sonata (1.5.2-1) unstable; urgency=low

  * New upstream version.
    - Fixes crash when MPD database is empty (Closes: #480102).
  * Move packaging to Python Applications Packaging Team:
    - Change Vcs fields in debian/control.
    - Add team to Uploaders.
  * Update to standards 3.8.0.

 -- Michal Čihař <nijel@debian.org>  Sat, 07 Jun 2008 12:11:10 +0200

sonata (1.5.1-1) unstable; urgency=low

  * New upstream version.
  * Drop double dh_pycentral call in debian/rules.

 -- Michal Čihař <nijel@debian.org>  Mon, 05 May 2008 11:59:28 +0200

sonata (1.5-4) unstable; urgency=low

  * Sonata works with python 2.4 and newer, tell it to python-central to
    create proper deps and avoid installation for older python versions
    (Closes: #476458).

 -- Michal Čihař <nijel@debian.org>  Wed, 16 Apr 2008 21:54:30 +0200

sonata (1.5-3) unstable; urgency=low

  * Build for all supported python versions (this makes sense at least for
    python-mmkeys).

 -- Michal Čihař <nijel@debian.org>  Mon, 14 Apr 2008 14:25:50 +0200

sonata (1.5-2) unstable; urgency=low

  * Depend on (not yet in unstable) package python-mpd.

 -- Michal Čihař <nijel@debian.org>  Mon, 07 Apr 2008 11:46:16 +0200

sonata (1.5-1) unstable; urgency=low

  * New upstream version.
    - Sets ZSI cache directory (Closes: #473801).
  * Build conflict with libffi4-dev as python-gobject-dev fails with it.

 -- Michal Čihař <nijel@debian.org>  Mon, 07 Apr 2008 11:31:40 +0200

sonata (1.4.2-2) unstable; urgency=low

  * Drop workaround for bug #452227 (Closes: #472028).

 -- Michal Čihař <nijel@debian.org>  Mon, 24 Mar 2008 21:16:31 +0100

sonata (1.4.2-1) unstable; urgency=low

  * New upstream version.

 -- Michal Čihař <nijel@debian.org>  Sun, 10 Feb 2008 14:45:30 +0900

sonata (1.4.1-1) unstable; urgency=low

  * New upstream version.
    - Fixes crash with disabled sys tray icon (Closes: #463579).
  * Delete empty /usr/lib (workaround for pycentral bug #452227).
  * Decrease sonata -> python-mmkeys dependency to recommends as this package
    is not needed on GNOME > 2.18,
  * Adjusted Vcs-* headers to point to trunk.
  * Drop not needed debian/patches.

 -- Michal Čihař <nijel@debian.org>  Mon, 04 Feb 2008 11:21:14 +0900

sonata (1.4-1) unstable; urgency=low

  * New upstream version.
    - MPD_HOST is not handled properly (Closes: #453249).
  * Upgrade suggests to recommends, as most users will want full
    featured player.
  * This package is GPL 3 or later.
  * Update policy to 3.7.3 (no changes needed).
  * Cleanup clean target in debian/rules.
  * No patches needed, dropped dpatch build dependency.
  * Install documentation only once (upstream now installes it also).
  * Split package to Arch:all sonata and Arch:any python-mmkeys.

 -- Michal Čihař <nijel@debian.org>  Wed, 16 Jan 2008 18:44:23 +0900

sonata (1.3-2) unstable; urgency=low

  * SOAPpy has been replaced by ZSI.

 -- Michal Čihař <nijel@debian.org>  Mon, 12 Nov 2007 15:31:19 +0900

sonata (1.3-1) unstable; urgency=low

  * New upstream version.
    - Desktop file patch was merged upstream.
    - Upstream now has own man page.
  * Use new Homepage field.
  * Convert XS-Vcs fields to Vcs.

 -- Michal Čihař <nijel@debian.org>  Tue, 06 Nov 2007 13:11:25 +0900

sonata (1.2.3-1) unstable; urgency=low

  * New upstream release.
  * Adpopted Debian menu to new policy.
  * Fix desktop file to match XDG.

 -- Michal Čihař <nijel@debian.org>  Thu, 30 Aug 2007 13:10:20 +0900

sonata (1.2.2-1) unstable; urgency=low

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Wed, 01 Aug 2007 09:44:12 +0200

sonata (1.2.1-1) unstable; urgency=low

  * New upstream release.
  	- Fixes MPD password error (Closes: #433440).

 -- Michal Čihař <nijel@debian.org>  Tue, 17 Jul 2007 13:14:12 +0200

sonata (1.2-1) unstable; urgency=low

  * New upstream version.
    - Adds smarter cover finder (Closes: #429331).
  * Suggests python-elementtree for Audioscrobbler support.

 -- Michal Čihař <nijel@debian.org>  Mon, 16 Jul 2007 18:30:03 +0200

sonata (1.1.1-1) unstable; urgency=low

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Mon, 21 May 2007 20:03:13 +0200

sonata (1.1-1) unstable; urgency=low

  * New upstream release.
    - Includes alternative icons for non GNOME users (Closes: #422877).
  * Patch to enable sessions is no longer needed.

 -- Michal Čihař <nijel@debian.org>  Tue, 08 May 2007 18:57:42 +0200

sonata (1.0.1-5) unstable; urgency=low

  * Do not build against experimental packages (Closes: #420123).

 -- Michal Čihař <nijel@debian.org>  Fri, 20 Apr 2007 10:45:28 +0200

sonata (1.0.1-4) unstable; urgency=low

  * Add XS-Vcs headers.
  * Fix suggests (Closes: #411565):
    - Remove python-taglib, no such thing exits, sorry.
    - python-tagpy is in NEW queue and should be soon available.
  * Use dpatch for patching upstream.

 -- Michal Čihař <nijel@debian.org>  Tue, 17 Apr 2007 09:07:46 +0200

sonata (1.0.1-3) unstable; urgency=low

  * Fix wrong sonata.py file, sorry for this error (Closes: #415014).
  * Use my Debian email address.

 -- Michal Čihař <nijel@debian.org>  Thu, 15 Mar 2007 15:05:45 +0100

sonata (1.0.1-2) unstable; urgency=low

  * Enabled GNOME session support (Closes: #409193).

 -- Michal Čihař <michal@cihar.com>  Wed, 14 Mar 2007 14:17:42 +0100

sonata (1.0.1-1) unstable; urgency=low

  * New upstream version.
  	- Should fix displying of log text (Closes: #409193).
  * It includes commented out Gnome session support, please enable and test it
    if you want to see this in next version (see bug #410286).

 -- Michal Čihař <michal@cihar.com>  Fri,  9 Mar 2007 10:11:28 +0100

sonata (1.0-1) unstable; urgency=low

  * New upstream version (Closes: #409457).
    - Includes fixed French translation (Closes: #405056).
  * Introduce new optional depenencies as Sonata can do more with them (I know
    some of these packages do not yet exist in Debian, but somebody will
    hopefully package them soon :-)).

 -- Michal Čihař <michal@cihar.com>  Mon,  5 Feb 2007 10:55:13 +0100

sonata (0.9-1) unstable; urgency=low

  * Initial release for Debian (Closes: #394201).

 -- Michal Čihař <michal@cihar.com>  Fri, 20 Oct 2006 11:51:51 +0200
